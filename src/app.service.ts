import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { Cache } from 'cache-manager';

@Injectable()
export class AppService {
  constructor(@Inject(CACHE_MANAGER) private readonly cacheManager: Cache) {}

  async getHello(): Promise<string> {
    await this.cacheManager.set(
      'cached_item',
      { key: 32 },
      { ttl: 10 }, // it will override global ttl
    );
    // await this.cacheManager.del('cached_item'); // will delete cached item
    // await this.cacheManager.reset(); // will remove all cached items
    const cachedItem = await this.cacheManager.get('cached_item');
    console.log(cachedItem);
    return 'Hello World!';
  }
}
